<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
  </head>
  <body>
    <div>
      <form action="crud.php" method="POST">
        <label for="create-first-name">Enter your first name:</label>
        <input type="text" id="create-first-name" name="first-name" />
        <br />
        <label for="create-middle-name">Enter your middle name:</label>
        <input type="text" id="create-middle-name" name="middle-name" />
        <br />
        <label for="create-last-name">Enter your last name:</label>
        <input type="text" id="create-last-name" name="last-name" />
        <br />
        <label for="create-birthday">Enter your birthday:</label>
        <input type="date" id="create-birthday" name="birthday" />
        <br />
        <label for="create-address">Enter your address:</label>
        <input type="text" id="create-address" name="address" />
        <br />
        <button type="submit" name="create">Create</button>
      </form>
    </div>

    <br />

    <div>
      <form action="crud.php" method="GET">
        <label for="read-all">Press to get all user info:</label>
        <button type="submit" id="read-all" name="read">Get Infos</button>
      </form>
    </div>

    <br />

    <div>
      <form action="crud.php" method="GET">
        <label for="get-user-id">Enter user id to fetch:</label>
        <input type="text" id="get-user-id" name="get-user-id" />
        <button type="submit" name="read-id">Get Infos</button>
      </form>
    </div>

    <br />

    <div>
      <form action="crud.php" method="POST">
        <label for="reference-user-id">Enter user id:</label>
        <input type="text" id="reference-user-id" name="id" />
        <br />
        <label for="update-first-name">Enter First Name:</label>
        <input type="text" id="update-first-name" name="first-name" />
        <br />
        <label for="update-middle-name">Enter Middle Name:</label>
        <input type="text" id="update-middle-name" name="middle-name" />
        <br />
        <label for="update-last-name">Enter Last Name:</label>
        <input type="text" id="update-last-name" name="last-name" />
        <br />
        <label for="update-birthday">Enter Birthday:</label>
        <input type="date" id="update-birthday" name="birthday" />
        <br />
        <label for="update-address">Enter Address:</label>
        <input type="text" id="update-address" name="address" />
        <br />
        <button type="submit" name="update-user">Update User</button>
      </form>
    </div>

    <br />

    <div>
      <form action="crud.php" method="POST">
        <label for="delete-user-id">Enter user id to delete:</label>
        <input type="text" id="delete-user-id" name="id" />
        <button type="submit" name="delete-id">Delete User</button>
      </form>
    </div>
  </body>
</html>
