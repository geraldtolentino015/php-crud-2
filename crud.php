<?php
require_once ('vendor\thingengineer\mysqli-database-class\MysqliDb.php');

$db = new MysqliDb ('localhost', 'root', '', 'pixel');

if (isset($_POST["create"])) {
  $firstName = $_POST["first-name"];
  $lastName = $_POST["last-name"];
  $middleName = $_POST["middle-name"];
  $birthday = $_POST["birthday"];
  $address = $_POST["address"];

  $data = Array ("first_name" => $firstName,
                "last_name" => $lastName,
                "middle_name" => $middleName,
                "birthday" => $birthday,
                "address" => $address);
  
  $id = $db->insert('employee', $data);
 
  if ($id) {
      echo "New record created successfully<br><br>";
  } else {
      echo "Error: " . $db->getLastErrno() . "<br><br>" . $db->getLastError();
  }
}


if (isset($_GET["read"])) {
  $cols = Array ("first_name", "last_name", "birthday");
  $users = $db->get("employee", null, $cols);

  if ($db->count > 0) {
      foreach ($users as $user) {
          echo "First Name: " . $user["first_name"] . " - Last Name: " . $user["last_name"] . " - Birthday: " . $user["birthday"]. "<br>";
      }
      echo "<br>";
  } else {
      echo "0 results<br>";
  }
}

if (isset($_GET["read-id"])) {
  $id = $_GET["get-user-id"];

  $cols = Array ("first_name", "last_name", "birthday");

  $db->where ("id", $id);
  $user = $db->getOne("employee", null, $cols);

  if ($user) {
    echo "First Name: " . $user["first_name"] . " - Last Name: " . $user["last_name"] . " - Birthday: " . $user["birthday"] . "<br>";
  } else {
    echo "No results found!";
  }
}

if (isset($_POST["update-user"])) {
  $firstName = $_POST["first-name"];
  $lastName = $_POST["last-name"];
  $middleName = $_POST["middle-name"];
  $birthday = $_POST["birthday"];
  $address = $_POST["address"];
  $id = $_POST["id"];

  $data = Array ("first_name" => $firstName,
                "last_name" => $lastName,
                "middle_name" => $middleName,
                "birthday" => $birthday,
                "address" => $address);
  
  $db->where ('id', $id);
  $updated = $db->update ('employee', $data);

  if ($updated) {
    echo "Record updated successfully<br><br>";
  } else {
    echo "Error: " . $db->getLastErrno() . "<br><br>" . $db->getLastError();
  }
}

if (isset($_POST["delete-id"])) {
  $id = $_POST["id"];

  $db->where("id", $id);
  $deleted = $db->delete('employee');

  if ($deleted) {
    echo "Record deleted successfully";
  } else {
      echo "Error deleting record: " . $db->getLastError();
  }
}

